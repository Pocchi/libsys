var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
  google_id: String,
  name: String
});

var BookSchema = new Schema({
  title: String,
  author: String,
  isbn: String,
  description: String,
  keyword: String,
  max_page: Number,
  user: [{ type: Schema.Types.ObjectId, ref: 'User' }],
}); 

var BookStatusSchema = new Schema({
  have: Boolean,
  like: Boolean,
  book: [{ type: Schema.Types.ObjectId, ref: 'Book' }],
  user: [{ type: Schema.Types.ObjectId, ref: 'User' }],
  progress: [{ type: Schema.Types.ObjectId, ref: 'Progress' }]
}); 

var ProgressSchema = new Schema({
  read_page: Number,
  book: [{ type: Schema.Types.ObjectId, ref: 'Book' }],
  user: [{ type: Schema.Types.ObjectId, ref: 'User' }]
}); 


exports.User = mongoose.model('User', UserSchema);
exports.Book = mongoose.model('Book', BookSchema);
exports.BookStatus = mongoose.model('BookStatus', BookStatusSchema);
exports.Progress = mongoose.model('Progress', ProgressSchema);
