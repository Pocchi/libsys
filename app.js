var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

// DBへの接続
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/libsys');

// モデルの宣言
var Model = require('./app/models/model');
var User = Model.User;
var Book = Model.Book;
var BookStatus = Model.BookStatus;
var Progress = Model.Progress;

//var routes = require('./routes/index');
//var users = require('./routes/users');


var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var router = express.Router();

router.use(function(req, res, next) {
  console.log('Something is happening.');
  next();
});


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/libsys/api', router);

router.get('/', function(req, res){
  res.json({message: 'sucsess'});
});

// users
router.route('/users')
//会員登録
.post(function(req, res){
  var user = new User();
  user.google_id = req.body.google_id;
  user.name = req.body.name;

  user.save(function(err){
    if (err){
      res.send(err);
    }
    res.json(user);
  });
})
.get(function(req, res) {
  User.find(function(err, users) {
    if (err){
      res.send(err);
    }
    res.json(users);
  });
});
//users:/id
router.route('/users/:user_id')
.get(function(req, res){
  User.find({ google_id: req.params.user_id }, function(err, user) {
    if (err){
      res.send(err);
    }
    res.json(user);
  });
});
//Book
router.route('/books')
.post(function(req, res){
  var book = new Book();
  book.title = req.body.title;
  book.author = req.body.author;
  book.isbn = req.body.isbn;
  book.keyword = req.body.keyword;
  book.max_page = req.body.max_page;
  book.user = req.body.user;

  book.save(function(err){
    if (err){
      res.send(err);
    }
    res.json(book);
  });
})
.get(function(req, res) {
  Book.find(function(err, books) {
    if (err){
      res.send(err);
    }
    res.json(books);
  });
});
//books:user_id ユーザー登録全検索(詳細のみ)
router.route('/books/register/:user_id')
.get(function(req, res){
  Book.find({ user: req.params.user_id }, function(err, books) {
    if (err){
      res.send(err);
    }
    res.json(books);
  });
});
router.route('/book/status')
//status登録
.post(function(req, res){
  var book_status = new BookStatus();
  book_status.have = req.body.have;
  book_status.like = req.body.like;
  book_status.book = req.body.book;
  book_status.user = req.body.user;
  book_status.progress = req.body.progress;

  book_status.save(function(err){
    if (err){
      res.send(err);
    }
    res.json(book_status);
  });

});
router.route('/book/status/:user_id')
// user指定検索 ステータス登録済み本詳細全件
.get(function(req, res){
  BookStatus.find({user: req.params.user_id})
  .populate('book')
  .populate('progress')
  .exec(function(err, book_statuses) {
    if (err){
      res.send(err);
    }
    res.json(book_statuses)
  });
});

router.route('/book/like/:user_id')
// user指定 like検索
.get(function(req, res){
  BookStatus.find({user: req.params.user_id, like:true})
  .populate('book')
  .populate('progress')
  .exec(function(err, book_statuses) {
    if (err){
      res.send(err);
    }
    res.json(book_statuses)
  });
});
router.route('/book/have/:user_id')
// user指定 have検索
.get(function(req, res){
  BookStatus.find({user: req.params.user_id, have:true})
  .populate('book')
  .populate('progress')
  .exec(function(err, book_statuses) {
    if (err){
      res.send(err);
    }
    res.json(book_statuses)
  });
});
router.route('/book/nohave/:user_id')
// user指定 nohave検索
.get(function(req, res){
  BookStatus.find({user: req.params.user_id, have:false})
  .populate('book')
  .populate('progress')
  .exec(function(err, book_statuses) {
    if (err){
      res.send(err);
    }
    res.json(book_statuses)
  });
});

router.route('/book/status/:status_id')
//status更新
.post(function(req, res) {
var book_status = req.body;
  BookStatus.findByIdAndUpdate(req.params.status_id, book_status, function(err, result) {
    if (err) {
      res.send({'error': 'An error has occurred - ' + err});
    } else {
      console.log('Success: ' + result + ' document(s) updated');
      res.send(book_status);
    }
  });


});
router.route('/book/progress')
//progress登録
.post(function(req, res){
  var progress = new Progress();
  progress.read_page = req.body.read_page;
  progress.user = req.body.user;
  progress.book = req.body.book;

  progress.save(function(err){
    if (err){
      res.send(err);
    }
    res.json(progress);
  });

})
.get(function(req, res) {
  Progress.find(function(err, progress) {
    if (err){
      res.send(err);
    }
    res.json(progress);
  });
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
